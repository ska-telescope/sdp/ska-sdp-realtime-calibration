SDP Realtime Calibration Processor
==================================

This package contains the implementation the SDP receiver processor 
for realtime calibration *RCalibration*.

Plus the calibration package business logic itself.

*Processors* are programs that connect to a Plasma store
and receive payloads from the receiver pipeline.

Development Requirements
------------------------

* Python 3.10 or greater
* Poetry 1.2.2 or greater

Clone the repository, then ``poetry install``
to install all the dependencies into a virtual environment.

To run the tests, execute ``make python-test``.


Docker
------

A docker image is generated in GitLab CI and stored in the SKAO CAR. The default docker image will run the RCalProcessor on startup with only the output file being required.

    docker compose build

Can be used to build a docker image.


The following will start an interactive RCalProcessor

    docker run --interactive --tty --rm --mount type=bind,source=/full-path-to-plasma-socket,destination=/plasma/socket plasma-processor-rcal <cal table output?>


An alternative processor can be run by changing the entrypoint

    docker run --interactive --tty --rm --mount type=bind,source=/full-path-to-plasma-socket --entrypoint <processor class> plasma-processor-rcal <-s plasma_socket> <cal table output?>
