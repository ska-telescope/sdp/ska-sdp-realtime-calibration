DOCS_SPHINXOPTS = -W --keep-going

# So we don't need all our tests to be in packages
# https://github.com/PyCQA/pylint/pull/5682
PYTHON_SWITCHES_FOR_PYLINT ?= --recursive=true
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=$(CURDIR)/src:$(CURDIR)/tests 
PYTHON_VARS_AFTER_PYTEST ?= --ignore tests/sky_model

include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/helm.mk


CASACORE_MEASURES_DIR ?= /usr/share/casacore/data
DOCKER_COMPOSE := docker compose


python-pre-build:
	pip install build

# We use GitlabCI services in CI so only use docker compose locally
python-pre-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-up \
		|| echo "Not starting docker-compose containers in CI"
	[[ -z $$GITLAB_CI ]] \
	    && python3 tests/wait_for_test_services.py --kafka-host ska-sdp-kafka:9092 \
		|| python3 tests/wait_for_test_services.py --kafka-host localhost:9092
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"

python-post-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-down \
		|| echo "Not stopping docker-compose containers in CI"

install-doc-requirements-with-poetry:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docker-compose-up:
	$(DOCKER_COMPOSE) --file docker/services.docker-compose.yml up --detach

docker-compose-down:
	$(DOCKER_COMPOSE) --file docker/services.docker-compose.yml down

docs-pre-build: install-doc-requirements-with-poetry
