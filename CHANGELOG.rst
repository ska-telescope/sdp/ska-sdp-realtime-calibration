Change Log
###########

Development
-----------

3.0.0
-----

Known issues
^^^^^^^^^^^^

* Interaction with the GSM service is currently broken
  due to changes in the response format and contents.

Added
^^^^^

* Visibility pre-processing.
* Support for sky and beam models.
* Visibility predict calls.
* Visibility calibration solver calls.

Changed
^^^^^^^

* **BREAKING** Bumped dependency on
  ``ska-sdp-datamodels``, ``ska-sdp-func`` and ``ska-sdp-func-python`` to ``^1.0.0``
   to support SDPv1.
  Accordingly, bump dependency on ``ska-sdp-realtime-receive-processors`` to ``^3.0.0``,
  and ``aiokafka`` to ``>=0.11.0,<0.13``.


2.0.1
-----
* Updated dependency on ``ska-sdp-realtime-receive-processors`` to ``^2.3.0``
  for compatibility against the config data base changes
* Updated dependency on ``ska-sdp-realtime-receive-core`` to ``~6.5.0``
  for compatibility against the config data base changes
* Updated the test dependency on ``ska-sdp-cbf-emulator`` to ``^8.3.0`` to 
  pick up the time epoch changes.
* Updated the test file to use an epoch to greater than 2000 to avoid the time epoch issues.    

2.0.0
-----

Changed
^^^^^^^

* Updated dependency on ``ska-sdp-realtime-receive-processors`` to ``^2.0.0``
  for compatibility against the latest receiver.

Removed
^^^^^^^

* **BREAKING** Removed custom ``max_calibration_intervals`` option
  in favour of the new, generic ``max_scans`` one.

1.0.0
-----

Changed
^^^^^^^

* Updated dependency on ``ska-sdp-realtime-receive-processors`` to ``^1.2.0``
  for compatibility against the latest receiver.

0.2.0
-----

Added
^^^^^

* Read the Kafka server host
  from the ``SDP_KAFKA_HOST`` environment variable
  if not already given.

0.1.0
-----

Repository built to house the new realtime calibration processor. This is based
on the original ska-sdp-realtime-receive-processors.

Added
^^^^^

* Added gain_table varible, of type GainTable, which is initialised from the
  input visibilties in the calibrate function.
* Added dependency on ska-sdp-func-python (version ^0.2.1)
* Added new function, resample, to apply func-python re-channelisation
  options for CBF beamformer output.
* Added BDD test for RCalProcessor running with visibility receive

Changed
^^^^^^^

* The get_calibration function returns the new gain_table variable.
* The calibrate function is no longer a @staticmethod.
* Added optional HDF5 file output to the RCalProcessor

Fixed
^^^^^

* Updated RCalProcessor calibrator_to_use: BaseCalibrator = RCalCalibrator()
