Getting started
===============

Installation
------------

To install this package use:
  ``pip install --extra-index-url=https://artefact.skao.int/repository/pypi-internal/simple ska-sdp-realtime-calibration``.

The RCalProcessor derives from :doc:`ska-sdp-realtime-receive-processors:index`
class :class:`~realtime.receive.processors.sdp.base_processor.BaseProcessor`
and requires a number of SKA SDP packages for real-time data access and
calibration routines:

  * :doc:`ska-sdp-realtime-receive-core:index`
  * :doc:`ska-sdp-realtime-receive-modules:index`
  * :doc:`ska-sdp-realtime-receive-processors:index`
  * :doc:`ska-sdp-datamodels:index`
  * :doc:`ska-sdp-func-python:index`

See the :doc:`ska-sdp-realtime-receive-processors:index` documentation for
detailed information about the class and how it interacts with other
processing components.


Running the RCal processor
--------------------------

The :doc:`ska-sdp-realtime-receive-processors:index` package provides the
program used to load and run the processors. This program is called
``plasma-processor``. It loads the user-supplied processor class and sets up
the necessary infrastructure to connect it to the Plasma store. See the
:doc:`processors documentation
<ska-sdp-realtime-receive-processors:getting_started>` for more information. 
With access to the Plasma store taken care of, a Kafka producer can be
enabled for transmission of gain arrays and provided to a new RCalProcessor:

.. code-block:: python

    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    producer.start()

    rcal_processor = RCalProcessor(
        max_calibration_intervals=NUM_TIMESTAMPS,
        rcal_producer=RCalProducer(
            producer,
            bandpass_topic,
        ),
    )

This processor can be used within a *Processor* runner:

.. code-block:: python

    rcal_runner = Runner(
        PLASMA_SOCKET,
        rcal_processor,
        polling_rate=0.001,
        use_sdp_metadata=False,
    )
 
and run alongside the Plasma store and receiver. Another processing
component, such as the CBF beamformer, can launch a Kafka consumer to
receive the gain arrays.


Gain arrays
-----------

The RCalProcessor fills a
:class:`~ska_sdp_datamodels.calibration.calibration_model.GainTable` dataset
with antenna-based gain solutions of jones_type "B". These
are re-channelised to have spectral sampling that is appropriate for CBF
beamforming, as determined by :doc:`SDP func-python<ska-sdp-func-python:index>`
calibration.beamformer_utils. To help determine what sampling is appropriate,
an additional RCalProcessor constructor argument, *array*, can be set to
"LOW" or "MID".

The CBF beamformer is expecting bandpass calibration Jones matrices,
sent as an array of complex floats with dimensions
(N\ :sub:`antenna`\ , N\ :sub:`frequency`\ , N\ :sub:`polarisation`\ ). These
are extracted from the re-channelised GainTable and sent to Kafka as a
BytesIO memory buffer:

.. code-block:: python

    nant = len(gaintable.antenna)
    nfreq = len(gaintable.frequency)
    npol = len(gaintable.receptor1) * len(gaintable.receptor2)
    bio = BytesIO()
    numpy.save(bio, gaintable.gain.data.reshape(nant, nfreq, npol))
    data = bio.getbuffer()
    producer.send_and_wait(bandpass_topic, data)


