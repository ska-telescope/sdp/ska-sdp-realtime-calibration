Calibration dataflow
====================

The initial implementation of the calibration setup is shown in the diagram
below (as of PI23). At this stage only the bandpass calibration has been
enabled, however a delay loop is also shown. This will likely be enabled for
AA0.5 to generate regular updates for the bandpass solutions.

Modification is still required in a number of places, some of which are
discussed below the diagram. In a few places, serial processing of frequencies
is shown. These indicate areas where changing to a parallel implementation
would have no impact on the logic of the underlying algorithm.

.. figure:: _static/img/RCal-PI23-flow.drawio.svg

Pre-processing
--------------

A number of pre-processing options have been tested in PI23 using
``ska_sdp_func_python`` ``preprocessing`` functions. These functions are in
development and not all have been included in the current version.

 * RFI masks: Applying pre-defined channel-range masks has been tested using
   ``rfi_masks.apply_rfi_masks``. This is not yet available in the main branch
   of ``ska_sdp_func_python``.
 * RFI flagging: Adaptive outlier flagging has been tested using
   ``flagger.rfi_flagger``. This is not yet available in the main branch of
   ``ska_sdp_func_python``.
 * Time and frequency averaging: Time and frequency averaging of received
   visibility datasets has been tested using ``averaging.averaging_frequency``
   and ``averaging.averaging_time``. Both of these are available. The receiver
   can be configured to average over a set number of samples, over the full
   time range, and/or to the Low coarse channel width.

Sky models
----------

The sky model database service is under development. The framework has been
added to the processor, however currently only a single point source at the
phase centre is added.

In addition to completing work on the sky model database, work is needed on
when and how to initialise and update both the database and local models. At
present this is all done at the start, after receiving the first dataset.
However, for large databases and continual processor operation, this will need
to be more adaptable. The following modifications seem reasonable:

 * The Global Sky Model is initialised once during processor initialisation and
   contains all sky model information required for real-time calibration.

 * The Local Sky Model is initialised and reset after each start-of-scan
   trigger. In the current workflow this would happen when processing the first
   dataset, so it would need to be fast. If required, the LSM could also be
   reset after each dataset is received.

Beam models
-----------

A beam class has been added for initialisation and sampling of antenna beam
models. The intention is to use ``Everybeam`` or ``func-python`` tools to
handle beam models, however these are not yet in place. The current placeholder
code extracts relevant information from the dataset, stores it in the beam
class and applies the resulting Jones matrices to sky model component
visibilities, however it currently just returns beam gains of unity for all
directions. This is adequate for the placeholder sky model that in place.

In addition to completing work on the beam models, work is also needed on when
and how they are initialised and reset. As with the sky model, at present this
is done once after receiving the first dataset, but will need to be more
adaptable. The following modifications seem reasonable:

 * Beam models are initialised once during processor initialisation. This could
   include setting up telescope information, loading element patterns, etc.

 * Beams are reset after each start-of-scan trigger. This could include setting
   parameters like frequencies and the pointing centre.

 * As in the current implementation, beams are resampled for every time step.

Bandpass calibration
--------------------

Bandpass calibration has been added. It is working as expected, however a
number changes may be needed for AA0.5:

 * Independent gain solutions are generated for each channel. In early array
   assemblies, each channel may not have enough bandwidth for reliable
   calibration. Channels can be broadened during pre-processing, however this
   will lead to decorrelation. If significant averaging is required, it may be
   beneficial to include multiple separate channels in a single solution
   interval. This is not yet implemented.

 * The default unpolarised ``ska_sdp_func_python`` solver is used. This will be
   changed to one of the polarised algorithms once the sky model is available.

Ionospheric delays
------------------

The ``ska_sdp_func_python`` ionospheric delay solver could also be used with
the bandpass calibration sky model.

Post-processing
---------------

A number of post-processing tasks are required before publishing the solutions.

 * Re-channelisation of gains to CBF spectral parameters is in place. This may
   need to be tweaked a little at the band edges to make sure all output
   channels are generated. There may also be benefit in coordinating this step
   with pre-processing channel averaging.

 * Need to determine what to do with solutions that are missing due to RFI
   flags. Should they be set to unity, left with their initial values, or
   interpolated? If they should be interpolated, is the re-channelisation doing
   the right thing?

 * The calibration Jones matrix solutions need to be combined with the beam
   model matrices before sending to CBF. Any ionospheric delays will also need
   to be included.

 * The combined Jones matrix solutions need to be inverted before sending to
   CBF.

Metadata
--------

What metadata should be made available? How is this done? Information on
entirely flagged antennas or channels should be communicated with CBF, QA
metrics may be useful for PSS/PST, etc.

