Real-Time Processor documentation
=================================

Processors
----------

.. Potential future in-built processors include no-op and logging processors
.. autoclass:: realtime.calibration.processors.rcal_processor.RCalProcessor
      :members:
      :special-members:

.. automodule:: realtime.calibration.processors.utilities
      :members:
      :special-members:

Calibrators
-----------

.. autoclass:: realtime.calibration.calibrators.base_calibrator.BaseCalibrator
      :members:
      :special-members:

.. autoclass:: realtime.calibration.calibrators.rcal_calibrator.RCalCalibrator
      :members:
      :special-members:

Sky, beam and visibility models
-------------------------------

.. autoclass:: realtime.calibration.sky_model.beams.GenericBeams
      :members:
      :special-members:

.. automodule:: realtime.calibration.sky_model.functions
      :members:
      :special-members:


