Realtime Calibration Library
============================

This package contains the *Realtime Calibration Pipeline*, a
:doc:`ska-sdp-realtime-receive-processors:index` *Processor* that generates
real-time calibration solutions for each visibility payload from the realtime
receive pipeline. The solutions are then made available for subsequent
real-time processing, in particular for tied-array beamforming in the
Correlator & Beamformer (CBF) of the Central Signal Processor.

The payloads are :class:`~ska_sdp_datamodels.visibility.vis_model.Visibility`
datasets that are sent from the *Receiver* via an Apache Plasma store. After
calibration, the solutions are extracted from the
:class:`~ska_sdp_datamodels.calibration.calibration_model.GainTable` *gain*
array and sent to Kafka data queues as a binary stream of dimension
(N\ :sub:`antenna`\ , N\ :sub:`frequency`\ , N\ :sub:`polarisation`\ =4).

Context
-------

At a high level the realtime receive processors lie at the end of the realtime
receive pipeline, each working with data written to the plasma store by the
*Receiver* (in the :doc:`ska-sdp-realtime-receive-modules:index` repository). 

The processors are responsible for performing some action on the data, for
example writing to a measurement set---and in this case, for performing
bandpass calibration. After calibration the antenna-based gains are
re-channelised to have spectral sampling that is appropriate for CBF
beamforming.


.. This diagram can be edited by opening the file directly in
.. https://app.diagrams.net
.. figure:: _static/img/RCal.drawio.svg


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   calibration_dataflow
   api


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
