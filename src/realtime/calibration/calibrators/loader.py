"""Utilities for dynamically locating calibrator classes"""

import importlib
import logging

from realtime.calibration.calibrators.base_calibrator import BaseCalibrator
from realtime.calibration.calibrators.rcal_calibrator import RCalCalibrator

logger = logging.getLogger(__name__)

_inbuilt_calibrators: dict[str, BaseCalibrator] = {
    "rcal": RCalCalibrator,
}


def get_calibrator_class(calibrator: str):
    """
    Resolve a calibrator either by inbuilt name or fully qualified class name
    """

    if consumer_class := _inbuilt_calibrators.get(calibrator.lower()):
        klass = consumer_class
    else:
        modname, classname = calibrator.rsplit(".", 1)
        module = importlib.import_module(modname)
        klass = getattr(module, classname)

    if not issubclass(klass, BaseCalibrator):
        logger.warning(
            "Resolved %s calibrator, but it is not a subclass of %s",
            klass,
            BaseCalibrator,
        )

    return klass
