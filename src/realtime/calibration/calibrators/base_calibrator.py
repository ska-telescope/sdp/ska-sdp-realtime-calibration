"""A base class to inherit from when implementing a calibrator"""

import logging

import numpy
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.calibration.calibration_model import GainTable
from ska_sdp_func_python.calibration.beamformer_utils import (
    resample_bandpass,
    set_beamformer_frequencies,
)

logger = logging.getLogger(__name__)


class BaseCalibrator:
    """A base class to inherit from when implementing a calibrator"""

    def __init__(self, config=NotImplemented):
        """_summary_"""
        self._config = config
        self.skycomponents = None
        self.model = None
        self.gain_table = None
        self.resampled_table = None

    def initialise_gaintable(self, vis, keep_gains=True):
        """Initialise the gaintable.

        Initialise the gaintable before calibration. This is done separately
        so that it can be skipped if cumulative solving is required.

        :param vis: Visibility data to be calibrated
        :param keep_gains: If true and the gain table exists, the existing
            gains will be retained. Default is True.
        """
        # set to false if it is impossible
        if self.gain_table is None:
            keep_gains = False

        # could skip create_gaintable_from_visibility and instead simply
        # update metadata.
        # but doing it the following way ensures consistent metadata
        if keep_gains:
            if not vis.frequency.equals(self.gain_table.frequency):
                raise ValueError("Table and vis have different frequencies")
            gain_data = self.gain_table.gain.data

        # generate a single gain table for the whole visibility
        # unfortunately, the more idiomatic vis.max("time") (and min) cannot
        # be used due to a bug in the Visibility class triggered by xarray
        # while trying to copy (?) the dataset. Newer versions of either
        # package might solve the issue
        full_visibility_timeslice = vis.time.data.max() - vis.time.data.min()
        self.gain_table = create_gaintable_from_visibility(
            vis, jones_type="B", timeslice=full_visibility_timeslice
        )

        if keep_gains:
            if numpy.any(gain_data.shape != self.gain_table.gain.shape):
                raise ValueError("New and existing tables are not equivalent")
            self.gain_table.gain.data = gain_data

    def reset_skymodel(self):
        """Whether or not to reset (or initialise) the sky model.

        - Need to initialise at the start, unless skycomponents is supplied at
          startup (not yet implemented).
        - May want to reset after observing changes, such as to the pointing
          direction, frequency or array configuration.
        - May want to reset very wide models often.

        For now, just do it once at the start.
        """
        return self.skycomponents is None

    def calibrate(self, vis):
        """Calibrate the data."""
        raise NotImplementedError

    def resample(self, array=None):
        """Resample the calibration solutions data

        The gain_table is re-channelised to have spectral sampling that
        is appropriate for CBF beamforming, as determined by SDP
        func-python calibration.beamformer_utils. The results are written
        into a new GainTable dataset.

        :param array: Array type for re-channelisation ("MID" or "LOW").
            Default = None
        """

        # set up the output frequency channels
        freq_out = set_beamformer_frequencies(self.gain_table, array=array)

        def log_frequency_band(role, frequencies):
            logger.debug(
                (
                    "%s band: f[0] = %.3f MHz ... f[%d] = %.3f MHz. "
                    "Channel width = %6.2f kHz"
                ),
                role,
                frequencies[0] / 1e6,
                len(frequencies) - 1,
                frequencies[-1] / 1e6,
                numpy.diff(frequencies)[0] / 1e3,
                stacklevel=2,
            )

        log_frequency_band("Input", self.gain_table.frequency.data)
        log_frequency_band("Output", freq_out)
        gains = resample_bandpass(freq_out, self.gain_table, alg="polyfit")

        # Set the gain weight to one and residual to zero
        # Will need to discuss how and if these should be set
        shape = gains.shape
        gain_weight = numpy.ones(shape)
        gain_residual = numpy.zeros((shape[0], shape[2], shape[3], shape[4]))

        self.resampled_table = GainTable.constructor(
            gain=gains,
            time=self.gain_table.time,
            interval=self.gain_table.interval,
            weight=gain_weight,
            residual=gain_residual,
            frequency=freq_out,
            receptor_frame=self.gain_table.receptor_frame1,
            phasecentre=self.gain_table.phasecentre,
            configuration=self.gain_table.configuration,
            jones_type=self.gain_table.jones_type,
        )

    def get_calibration(self) -> GainTable:
        """Return the calibration gain_table.

        If it has been resampled, the resampled version is returned.

        :return: GainTable
        """
        if self.resampled_table is not None:
            return self.resampled_table

        return self.gain_table
