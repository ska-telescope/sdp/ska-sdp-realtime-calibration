"""The real-time calibrator example"""

import logging

from ska_sdp_datamodels.calibration.calibration_model import GainTable
from ska_sdp_datamodels.visibility.vis_model import Visibility
from ska_sdp_func_python.calibration.solvers import solve_gaintable

from realtime.calibration.calibrators.base_calibrator import BaseCalibrator

logger = logging.getLogger(__name__)


class RCalCalibrator(BaseCalibrator):
    """Realtime calibration support class.

    Child of BaseCalibrator.
    """

    def calibrate(self, vis):
        """Calibrate the data.

        Generate bandpass calibration solutions using ska_sdp_func_python
        function solve_gaintable.

        :param vis: Visibility data to calibrate
        """

        if not isinstance(vis, Visibility):
            raise ValueError(f"Data is not of type Visibility: {type(vis)}")
        logger.debug(
            "Calibrating time interval %s, f[0] = %.3f ... f[%d] = %.3f MHz",
            vis.datetime.data[0],
            vis.frequency.data[0] / 1e6,
            len(vis.frequency) - 1,
            vis.frequency.data[-1] / 1e6,
        )

        if self.gain_table is None:
            raise ValueError("gaintable not initialised")
        if not isinstance(self.gain_table, GainTable):
            raise ValueError("gaintable is not of type GainTable")
        if self.gain_table.sizes["time"] != 1:
            raise ValueError(f"wrong time length: {len(self.gain_table.time)}")
        if len(self.gain_table.frequency) != len(vis.frequency):
            raise ValueError(
                f"wrong frequency length: {len(self.gain_table.frequency)}"
            )

        # It may be useful to have multi-channel solution intervals,
        # particularly for AA0.5. Neither jones_type="B" nor jones_type="G" can
        # do this. Instead, we may need a loop over the multi-channel sub-bands
        # and call solve_gaintable for each with jones_type="G". Probably also
        # need to move initialise_gaintable out of base_calibrator.

        solver = "gain_substitution"
        # solver = "jones_substitution"
        # solver = "normal_equations"
        # solver = "normal_equations_presum"

        self.gain_table = solve_gaintable(
            vis=vis,
            modelvis=self.model,
            gain_table=self.gain_table,
            solver=solver,
            phase_only=False,
            niter=200,
            tol=1e-06,
            crosspol=False,
            normalise_gains=None,
            jones_type="B",
            timeslice=None,
            refant=0,
        )

        # test this at some point (check params as they haven't been tested):
        #
        # xyz = vis.configuration.xyz.data
        # # separate cluster for each antenna (i.e. antenna-based delays)
        # cluster_id = numpy.arange(len(xyz))
        # zernike_limit = numpy.zeros(len(xyz))
        # # or a single cluster (small array or triangle of baselines)
        # cluster_id = numpy.zeros(len(xyz))
        # zernike_limit = None
        #
        # from ska_sdp_func_python.calibration.ionosphere_solvers import (
        #     solve_ionosphere
        # )
        # self.gain_table = solve_ionosphere(
        #     vis=vis,
        #     modelvis=self.model,
        #     xyz=xyz,
        #     cluster_id=cluster_id,
        #     zernike_limit=zernike_limit,
        #     block_diagonal=True,
        #     niter=20,
        # )
