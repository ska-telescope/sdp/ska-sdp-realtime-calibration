"""Built-in NullProcessor implementation"""

# Clean up the arguments when there is a better idea of what they all are
# pylint: disable=too-many-arguments

import argparse
import logging
import os
from io import BytesIO
from typing import Iterable, Optional

import numpy
from aiokafka import AIOKafkaProducer
from realtime.receive.processors.sdp.base_processor import BaseProcessor
from ska_sdp_datamodels.calibration.calibration_functions import (
    export_gaintable_to_hdf5,
)
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.calibration.calibrators.loader import get_calibrator_class
from realtime.calibration.calibrators.rcal_calibrator import (
    BaseCalibrator,
    RCalCalibrator,
)
from realtime.calibration.processors.utilities import pre_process
from realtime.calibration.sky_model.beams import GenericBeams
from realtime.calibration.sky_model.functions import (
    generate_lsm,
    predict_model_vis,
)

logger = logging.getLogger(__name__)


class RCalProcessor(BaseProcessor):
    """A processor that can present Visibility data to the RCal library.
    Child of BaseProcessor.

    Attributes
    ----------
    calibrator_to_use: BaseCalibrator
        Calibration support class to use. Default = RCalCalibrator()
    kafka_server: Optional[str]
        Bootstrap server for RCal Kafka producer.
        Default = None (i.e. no Kafka output)
    kafka_topic: Optional[str]
        Kafka topic for RCal Kafka producer.
        Default = None (i.e. no Kafka output)
    output_hdf5: Optional[str]
        Output HDF5 gain file, mostly used for debugging.
        Default = None
    array: Optional[str]
        Explicitly set the array type for re-channelisation ("MID" or "LOW").
        Default = None
    preproc_ave_time: Optional[int]
        Pre-average visibility dataset in time by this number of samples.
        See :func:`~realtime.calibration.processors.utilities.pre_process`
        for defaults.
    preproc_ave_frequency: Optional[int]
        Pre-average visibility dataset in frequency by this number of channels.
        See :func:`~realtime.calibration.processors.utilities.pre_process`
        for defaults.

    :param BaseProcessor: processor base class
    """

    def __init__(
        self,
        calibrator_to_use: BaseCalibrator = RCalCalibrator(),
        kafka_server: Optional[str] = None,
        kafka_topic: Optional[str] = None,
        output_hdf5: Optional[str] = None,
        array: Optional[str] = None,
        preproc_ave_time: Optional[bool] = None,
        preproc_ave_frequency: Optional[bool] = None,
    ):  # pylint: disable=too-many-positional-arguments
        super().__init__()
        self._intervals_processed = 0
        self._calibrator = calibrator_to_use
        # Needs to be reset between unit tests
        self._calibrator.__init__()
        self._output_hdf5 = output_hdf5
        self._array = array
        self._timestep = preproc_ave_time
        self._freqstep = preproc_ave_frequency
        self._beams = None
        self._kafka_server = kafka_server or os.getenv("SDP_KAFKA_HOST")
        self._kafka_topic = kafka_topic
        self._kafka_producer: AIOKafkaProducer | None = None

    @staticmethod
    def create(argv: Iterable[str]) -> "RCalProcessor":
        parser = argparse.ArgumentParser(
            description=(
                "A processor that presents Visibility data to the RCal library"
            ),
            prog="RCalProcessor",
        )
        parser.add_argument(
            "--calibrator",
            type=get_calibrator_class,
            default=RCalCalibrator,
            help="The calibrator to use. Must be a subclass of BaseCalibrator",
        )
        parser.add_argument(
            "--kafka-server",
            type=str,
            default=None,
            help="Bootstrap server for RCal AIOKafkaProducer",
        )
        parser.add_argument(
            "--kafka-topic",
            type=str,
            default=None,
            help="Kafka topic for RCal AIOKafkaProducer",
        )
        parser.add_argument(
            "--output-hdf5",
            type=str,
            default=None,
            help="Output HDF5 gain file, mostly used for debugging",
        )
        parser.add_argument(
            "--array",
            type=str,
            default=None,
            help="Set array type to MID or LOW for re-channelisation",
        )
        parser.add_argument(
            "--preproc_ave_time",
            type=int,
            default=None,
            help="Pre-average vis in time by this number of samples",
        )
        parser.add_argument(
            "--preproc_ave_frequency",
            type=int,
            default=None,
            help="Pre-average vis in time by this number of channels",
        )
        args = parser.parse_args(argv)
        return RCalProcessor(
            calibrator_to_use=args.calibrator(),
            kafka_server=args.kafka_server,
            kafka_topic=args.kafka_topic,
            output_hdf5=args.output_hdf5,
            array=args.array,
            preproc_ave_time=args.preproc_ave_time,
            preproc_ave_frequency=args.preproc_ave_frequency,
        )

    async def process(self, _dataset: Visibility) -> bool:
        # Visibility data sent from the receiver have a phasecentre
        # with ndim = 2, but export_gaintable_to_hdf5 expects ndim = 0.
        # Possible reasons for the extra dimensions:
        #  - Multiple fields/beams/.... Should throw an exception.
        #  - Ephemeris info (i.e. temporal derivatives). Should be passed.
        # For now just get the first one and warn if there are more
        if _dataset.phasecentre.size > 1:
            logger.warning("warning: received phasecentre is multidimensional")
        _dataset.attrs["phasecentre"] = _dataset.phasecentre[0, 0]

        logger.info(
            "RCalProcessor processor run %d", self._intervals_processed
        )

        vis = pre_process(
            vis=_dataset, timestep=self._timestep, freqstep=self._freqstep
        )

        # The following initialisations should be made aware of the start of
        # scan trigger.

        # Reset (or initialise) the gain dataset
        self._calibrator.initialise_gaintable(vis=vis, keep_gains=True)

        # Reset (or initialise) the LSM component list?
        if self._calibrator.reset_skymodel():
            self._calibrator.skycomponents = generate_lsm(vis=vis)

        # Reset (or initialise) the beam models?
        if self._beams is None:
            # put in a function once there are more realistic beam types
            self._beams = GenericBeams(vis=vis, array=self._array)

        # Generate the model visibilities for the current time step
        self._calibrator.model = predict_model_vis(
            vis=vis,
            skycomponents=self._calibrator.skycomponents,
            beams=self._beams,
        )

        # Generate gain solutions for the current time step
        self._calibrator.calibrate(vis=vis)

        # Dump this gain table to a HDF5 file?
        if self._output_hdf5 is not None:
            table = self._calibrator.get_calibration()
            export_gaintable_to_hdf5(table, self._output_hdf5)

        # Send this gain table to the Kafka data queues?
        if self._kafka_server and self._kafka_topic:
            # Re-channelise the gain solutions for CBF the beamformer
            self._calibrator.resample(array=self._array)

            # If this is the first time, start the Kafka producer
            if self._intervals_processed == 0:
                self._kafka_producer = AIOKafkaProducer(
                    bootstrap_servers=self._kafka_server,
                )
                await self._kafka_producer.start()

            # Set up a transmit task and send the data
            #  - The CBF beamformer requires complex arrays with dimensions
            #    [nant, nfreq, npol=4]. See ADR-58 and SP-2994.
            table = self._calibrator.get_calibration()
            nant = len(table.antenna)
            nfreq = len(table.frequency)
            npol = len(table.receptor1) * len(table.receptor2)
            # Save gains to a memory buffer
            bio = BytesIO()
            numpy.save(
                bio,
                table.gain.data.reshape(nant, nfreq, npol).view(numpy.float64),
            )
            data = bio.getbuffer()

            # Send the buffered data to the Kafka data queues
            await self._kafka_producer.send_and_wait(
                self._kafka_topic,
                data,
            )

        self._intervals_processed += _dataset.sizes["time"]

    async def close(self):
        if self._kafka_producer is not None:
            await self._kafka_producer.stop()
