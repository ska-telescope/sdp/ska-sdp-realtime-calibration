"""Processor helper functions"""

__all__ = [
    "pre_process",
]

import logging

import numpy
from ska_sdp_datamodels.visibility.vis_model import Visibility
from ska_sdp_func_python.preprocessing.averaging import (
    averaging_frequency,
    averaging_time,
)
from ska_sdp_func_python.preprocessing.flagger import rfi_flagger
from ska_sdp_func_python.preprocessing.rfi_masks import apply_rfi_masks

logger = logging.getLogger(__name__)


def pre_process(
    vis: Visibility,
    timestep: int = None,
    freqstep: int = None,
) -> Visibility:
    """Pre-process a visibility dataset.

    :param vis: Visibility data to be processed
    :param timestep: Optional[int]
        Pre-average visibility dataset in time by this number of samples. If
        preproc_ave_time = -1, average over all time steps.
    :param freqstep: Optional[int]
        Pre-average visibility dataset in frequency by this number of channels.
        If preproc_ave_frequency = -1, average to the Low CBF resolution of
        781.25 kHz.
    :return: Pre-processed the Visibility dataset
    """
    if not isinstance(vis, Visibility):
        raise ValueError(f"Data is not of type Visibility: {type(vis)}")

    # Flag known bad channels
    # rfi_frequency_masks is a 2D array of [start,end] frequencies
    # rfi_frequency_masks = numpy.array([[137.4e6, 137.6e6]])
    rfi_frequency_masks = None
    vis = apply_rfi_masks(vis, rfi_frequency_masks)

    # Adaptive RFi flagging (requires updates to func and func-python)
    vis = rfi_flagger(vis)

    # Speed up processing by pre-averaging over the solution interval?
    if freqstep is not None and len(vis.frequency) > 1:
        # auto-update freqstep if = -1
        if freqstep == -1:
            dfrequency_bf = 781.25e3
            dfrequency = vis.frequency.data[1] - vis.frequency.data[0]
            freqstep = int(numpy.round(dfrequency_bf / dfrequency))
        if freqstep > 1:
            vis = averaging_frequency(vis, freqstep=freqstep)

    if timestep is not None and len(vis.time) > 1:
        # auto-update timestep if = -1
        if timestep == -1:
            timestep = len(vis.time)
        if timestep > 1:
            vis = averaging_time(vis, timestep=timestep)

    return vis
