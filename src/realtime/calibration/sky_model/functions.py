"""Functions for generating sky models and model visibilities"""

__all__ = [
    "generate_lsm",
    "predict_model_vis",
]

import logging
from typing import List

import numpy
from astropy import units
from astropy.coordinates import AltAz, SkyCoord
from astropy.time import Time
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_datamodels.visibility.vis_model import Visibility
from ska_sdp_func_python.imaging.dft import dft_skycomponent_visibility

from .gsm_utils import (
    LocalSkyModelRequest,
    convert_model_to_skycomponents,
    request_local_sky_model,
)

logger = logging.getLogger(__name__)


def lsm_from_gsm(vis, fov=5.0, freq0=200e6) -> List[SkyComponent]:
    """
    Generate a Local Sky Model from the Global Sky Model.
    The model is generated at the phase centre of the visibility data.
    The FOV defaults to 5 degrees.
    The Reference Frequency for flux scaling is 200 MHz.

    :param vis: Visibility data to be calibrated
    :param fov: Field of view in degrees
    :param freq0: Reference frequency for flux scaling

    :return SkyComponent list
    """
    request = LocalSkyModelRequest(
        ra=vis.phasecentre.ra.degree,
        dec=vis.phasecentre.dec.degree,
        flux_wide=0.0,
        telescope="MWA",
        fov=fov,
    )

    model = request_local_sky_model(request)
    skycomponents = convert_model_to_skycomponents(model, freq0)

    return skycomponents


def generate_lsm(vis, fov=5.0, use_sky_model=False):
    """Initialise the local sky model.

    Placeholder for the proper sky model.

    For a large number of components or for significant diffuse emission,
    the full SkyModel class could be used instead. This consists of an
    Image and a SkyComponent list. For now RCal will be kept light weight.

    :param vis: Visibility data to be calibrated
    :param fov: Field of view in degrees (only used if use_sky_model is True)
    :param use_sky_model: Use the sky model service to generate the sky model
    :return: SkyComponent List
    """
    if not isinstance(vis, Visibility):
        raise ValueError(f"Data is not of type Visibility: {type(vis)}")

    skycomponents = []

    freq = vis.frequency.data
    freq0 = 200e6

    direction = SkyCoord(
        ra=vis.phasecentre.ra.hour,
        dec=vis.phasecentre.dec.degree,
        unit=(units.hourangle, units.deg),  # pylint: disable=no-member
    )
    flux0 = 1
    alpha = 0
    flux = numpy.zeros((len(freq), len(vis.polarisation)))
    flux[:, 0] = flux[:, 3] = flux0 / 2 * (freq / freq0) ** alpha
    skycomponents.append(
        SkyComponent(
            direction=direction,
            frequency=freq,
            name="point_source",
            flux=flux,
            shape="Point",
            polarisation_frame=PolarisationFrame("linear"),
        ),
    )  # pylint: disable=duplicate-code

    if use_sky_model:
        # this updates the sky model in the database.
        # this is only needed once. Unless the catalog is changed.
        skycomponents = lsm_from_gsm(vis, fov=fov, freq0=freq0)

    return skycomponents


def predict_model_vis(vis, skycomponents, beams=None):
    """Predict model visibilities.

    :param vis: Visibility dataset to be modelled
    :param skycomponents: SkyComponent List containing the local sky model
    :param beams: Optional GenericBeams object. Defaults to no beams.
    :return: Visibility model
    """
    if not isinstance(vis, Visibility):
        raise ValueError(f"Data is not of type Visibility: {type(vis)}")
    model = vis.copy(deep=True)

    if beams is None:
        # If there is no direction-dependent beam, do all components together
        model = dft_skycomponent_visibility(model, skycomponents)

    else:
        # Otherwise do predict the components one at a time

        # Check beam pointing direction
        time = Time(vis.datetime.data[0])
        altaz = beams.beam_direction.transform_to(
            AltAz(obstime=time, location=beams.array_location)
        )
        if altaz.alt.degree < 0:
            logger.warning("pointing below horizon: %.f deg", altaz.alt.degree)

        ntime = len(vis.time)
        nbl = len(vis.baselines)
        nchan = len(vis.frequency)

        model.vis.data = numpy.zeros(model.vis.shape)
        npredict = 0
        for comp in skycomponents:
            # Check component direction
            altaz = comp.direction.transform_to(
                AltAz(obstime=time, location=beams.array_location)
            )
            if altaz.alt.degree < 0:
                logger.warning("LSM component [%s] below horizon", comp.name)
                continue
            # Predict model visibilities for component
            compvis = model.copy(deep=True)
            compvis = dft_skycomponent_visibility(compvis, skycomponents)
            # Apply beam distortions and add to combined model visibilities
            response = beams.array_response(
                direction=comp.direction, time=time
            )
            # This is overkill if the beams are the same for all antennas...
            model.vis.data = (
                model.vis.data
                + numpy.einsum(  # pylint: disable=too-many-function-args
                    "bfpx,tbfxy,bfqy->tbfpq",
                    response[compvis.antenna1.data, :, :, :],
                    compvis.vis.data.reshape(ntime, nbl, nchan, 2, 2),
                    response[compvis.antenna2.data, :, :, :].conj(),
                ).reshape(ntime, nbl, nchan, 4)
            )
            npredict += 1

        logger.debug("%s sky model components predicted", npredict)
        if npredict == 0:
            logger.warning("No sky model components predicted")

    return model
