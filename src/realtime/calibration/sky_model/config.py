""" Model configuration for sky model API """

from typing import List, Optional

from pydantic import BaseModel, Field
from ska_sdp_global_sky_model.api.app.model import NarrowBandData, WideBandData

GSM_DEFAULT_HOST = (
    "http://ska-sdp-global-sky-model.dp-shared.svc.cluster.local"
)


class LocalSkyModelRequest(BaseModel):
    """Request model for Local Sky Model. This model is used to request the
    local sky model for a given position and telescope."""

    ra: float = Field(0.0, description="Right Ascension")
    dec: float = Field(0.0, description="Declination")
    flux_wide: float = Field(0.0, description="Flux density in wide band")
    telescope: str = Field("MWA", description="Telescope name")
    fov: float = Field(0.0, description="Field of view")

    class Config:  # pylint: disable=too-few-public-methods
        """Configuration for the model"""

        arbitrary_types_allowed = True


class LocalSkyModelResponse(BaseModel):
    """The response model for Local Sky Model - this is the model that is
    returned by the API when a request is made for the local sky model"""

    ra: Optional[float] = None
    dec: Optional[float] = None
    narrowband: List[NarrowBandData] = Field(default_factory=list)
    wideband: List[WideBandData] = Field(default_factory=list)

    class Config:  # pylint: disable=too-few-public-methods
        """Configuration for the model"""

        arbitrary_types_allowed = True


class LocalSkyModel(BaseModel):
    """Pydantic model for our internal representation of the local sky model"""

    sources: List[LocalSkyModelResponse] = Field(default_factory=list)

    class Config:  # pylint: disable=too-few-public-methods
        """model configuration"""

        arbitrary_types_allowed = True
