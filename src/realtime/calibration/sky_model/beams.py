"""Functions for generating sky models and model visibilities"""

import logging

import numpy
from astropy import units
from astropy.coordinates import AltAz, EarthLocation, SkyCoord
from astropy.time import Time
from ska_sdp_datamodels.visibility.vis_model import Visibility

logger = logging.getLogger(__name__)


class GenericBeams:
    """A generic class for beam handling.

    Generic interface to beams.
    Beams based on everybeam or other packages will be added or derived.
    Expect updates once the other packages are integrated. Everybeam, for
    instance, stores a lot of the same information as this class.
    Function array_response should not change too much though.

    At present, all beam values are set to 2x2 identity matrices. This
    demonstrates the usage and interfaces, but otherwise has no effect.
    """

    def __init__(
        self,
        vis: Visibility,
        array: str = None,
        direction: SkyCoord = None,
    ):
        if not isinstance(vis, Visibility):
            raise ValueError(f"Data is not of type Visibility: {type(vis)}")

        # Can relax this, but do it like this for now...
        if vis._polarisation_frame != "linear" or len(vis.polarisation) != 4:
            raise ValueError("Beams are only defined for linear data.")

        if direction is None:
            self.beam_direction = vis.phasecentre

        # Useful metadata
        self.antenna_names = vis.configuration.names.data
        self.nantenna = len(self.antenna_names)
        self.frequency = vis.frequency.data
        self.nfrequency = len(self.frequency)
        self.array_location = vis.configuration.location
        self.antenna_locations = []
        for antenna in range(self.nantenna):
            xyz = vis.configuration.xyz.data[antenna, :]
            self.antenna_locations.append(
                EarthLocation.from_geocentric(
                    xyz[0],
                    xyz[1],
                    xyz[2],
                    unit=units.m,  # pylint: disable=no-member
                )
            )

        # Check beam pointing
        altaz = self.beam_direction.transform_to(
            AltAz(
                obstime=Time(vis.datetime.data[0]),
                location=self.array_location,
            )
        )
        if altaz.alt.degree < 0:
            logger.warning("pointing below horizon: %.f deg", altaz.alt.degree)

        if array is None:
            name = vis.configuration.name.lower()
            if name.find("low") >= 0:
                array = "low"
            elif name.find("mid") >= 0:
                array = "mid"
            else:
                array = ""
        if array.lower() == "low":
            logger.info("Initialising beams for Low")
            self.array = array.lower()
        elif array.lower() == "mid":
            logger.info("Initialising beams for Mid")
            self.array = array.lower()
        else:
            logger.info("Unknown beam")

    def update_beam_direction(self, direction: SkyCoord):
        """Return the response of each antenna or station in a given direction.

        :param direction: Pointing direction of the beams
        """
        self.beam_direction = direction

    def array_response(self, direction: SkyCoord, time: Time = None):
        """Return the response of each antenna or station in a given direction

        :param direction: Direction of desired response
        :return: array of beam matrices [nant, nfreq, 2, 2]
        """
        if time is not None:
            altaz = direction.transform_to(
                AltAz(obstime=time, location=self.array_location)
            )
            if altaz.alt.degree < 0:
                logger.warning(
                    "Direction below horizon. Returning unit gains."
                )

        beams = numpy.empty((self.nantenna, self.nfrequency, 2, 2), "complex")
        beams[..., :, :] = numpy.eye(2)

        return beams
