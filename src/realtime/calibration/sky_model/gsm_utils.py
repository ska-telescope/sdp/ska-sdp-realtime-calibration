""" Utilities for the GSM API """

import logging
import os
from typing import List

import httpx
import numpy
from astropy import units
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_global_sky_model.api.app.model import NarrowBandData, WideBandData

from .config import (
    GSM_DEFAULT_HOST,
    LocalSkyModel,
    LocalSkyModelRequest,
    LocalSkyModelResponse,
)

logger = logging.getLogger(__name__)


def convert_request_to_params(request: LocalSkyModelRequest) -> dict:
    """Convert the request model to a dictionary of parameters for the API"""
    params = {}
    if request is not None:
        params = {
            "ra": request.ra,
            "dec": request.dec,
            "flux_wide": request.flux_wide,
            "telescope": request.telescope,
            "fov": request.fov,
        }
    return params


def convert_results_to_model(results):
    """Convert the results from the API to a LocalSkyModel model"""

    sources = []

    for (
        hpx_key,
        sources_dict,
    ) in results.items():

        logger.info("HPX Key: %s", hpx_key)
        if not isinstance(sources_dict, dict):
            raise ValueError("Sources should be a dictionary")

        for source_key, source_values in sources_dict.items():
            logger.info("Source Key: %s", source_key)

            if not isinstance(source_values, dict):
                raise ValueError("Source values should be a dictionary")

            for source_id, source_value in source_values.items():
                logger.info("Source ID: %s", source_id)
                nbanddata = []
                wbanddata = []

                for nband in source_value["narrowband"]:
                    nbanddata.append(NarrowBandData(**nband))
                for wband in source_value["wideband"]:
                    wbanddata.append(WideBandData(**wband))
                response = LocalSkyModelResponse(
                    ra=source_value["ra"],
                    dec=source_value["dec"],
                    wideband=wbanddata,
                    narrowband=nbanddata,
                )
                sources.append(response)

    return LocalSkyModel(sources=sources)


async def get_local_sky_model(api_url: str, params: dict = None):
    """Get the local sky model from the GSM API"""
    try:
        async with httpx.AsyncClient() as client:
            rcal_cmd = api_url + "/local_sky_model"
            response = await client.get(url=rcal_cmd, params=params)
            if response.status_code == 200:
                response_data = response.json()

                local_sky_model = convert_results_to_model(response_data)

                return local_sky_model

            logger.error("Error getting RCAL")
            raise httpx.RequestError(
                f"Error getting RCAL: {response.status_code}"
            )
    except httpx.RequestError as e:
        print(f"Error making request: {e}")
        raise e


async def request_local_sky_model(
    request: LocalSkyModelRequest,
) -> LocalSkyModel:
    """Request the local sky model from the GSM API"""
    params = request.model_dump()
    sky_model_url = os.getenv("GSM_HOST")

    if sky_model_url is None:
        sky_model_url = GSM_DEFAULT_HOST
        logger.warning(
            "SKY_MODEL_URL is not set using default: %s\n", sky_model_url
        )

    api = f"{sky_model_url}"

    try:
        response_model = await get_local_sky_model(api_url=api, params=params)
    except httpx.RequestError as e:
        logger.error("Error making request: %s", params.to_string())
        raise e

    return response_model


def convert_model_to_skycomponents(
    model: LocalSkyModel, freq: List[float], freq0: float = 200e6
) -> List[SkyComponent]:
    """Convert the LocalSkyModel to a list of SkyComponents"""
    skycomponents = []
    freq = numpy.array(freq)

    for source in model.sources:
        alpha = source.wideband[0].Spectral_Index
        flux0 = source.wideband[0].Int_Flux_Wide

        # assume 4 pols
        flux = numpy.zeros((len(freq), 4))
        flux[:, 0] = flux[:, 3] = flux0 / 2 * (freq / freq0) ** alpha
        # pylint: disable=no-member, duplicate-code
        skycomponents.append(
            SkyComponent(
                direction=SkyCoord(
                    ra=source.ra,
                    dec=source.dec,
                    unit=(units.deg, units.deg),
                ),
                frequency=freq,
                name="point_source",
                flux=flux,
                shape="Point",
                polarisation_frame=PolarisationFrame("linear"),
            )
        )

    return skycomponents
