"""Simple script to verify Kafka can be queried"""

import argparse
import asyncio
import logging
import time

import requests
from aiokafka import AIOKafkaProducer
from aiokafka.errors import KafkaError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

KAFKA_HOST = "localhost:9092"
DEFAULT_GSM_HOST = (
    "http://ska-sdp-global-sky-model.dp-shared.svc.cluster.local"
)
TIMEOUT_SECS = 10.0
WAIT_ON_FAIL_SECS = 0.5


async def amain(coroutines_to_await):
    """Async entry point"""
    for co in coroutines_to_await:
        await co


def _wait_for_gsm(gsm_host):
    """Wait for Sky Model Service to be accessible"""

    logger.info("Attempting to connect to Sky Model Service@%s", gsm_host)
    start = time.perf_counter()

    while True:
        try:
            api = f"{gsm_host}/openapi.json"
            response = requests.get(api, timeout=TIMEOUT_SECS)
            if response.status_code == 200:
                logger.info(
                    "Successfully connected to Sky Model Service@%s",
                    api,
                )
                break

            logger.error(
                "Failed to connect to GSM@%s, status code: %s",
                api,
                response.status_code,
            )
        except requests.RequestException as exc:
            logger.error(
                "An error occurred connecting to Sky Model Service@%s: %s",
                api,
                exc,
            )

        if time.perf_counter() - start >= TIMEOUT_SECS:
            raise TimeoutError(
                "Waited too long for Sky Model Service to be accessible"
            )
        time.sleep(WAIT_ON_FAIL_SECS)


async def _wait_for_kafka(kafka_host):
    logger.info("Attempting to connect to Kafka@%s", kafka_host)

    start = time.perf_counter()
    client = AIOKafkaProducer(bootstrap_servers=kafka_host)

    while True:
        try:
            await client.start()
            logger.info("Connected to Kafka")
            break
        except KafkaError as exc:
            logger.info("Failed to connect to Kafka")
            await asyncio.sleep(WAIT_ON_FAIL_SECS)
            if time.perf_counter() - start >= TIMEOUT_SECS:
                raise TimeoutError(
                    "Waited too long for Kafka to be accessible"
                ) from exc

    await client.stop()


def parse_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
        description="Wait for services to be accessible"
    )
    parser.add_argument(
        "--services",
        nargs="+",
        choices=["kafka", "gsm"],
        required=False,
        default=["kafka"],
        help="List of services to wait for (e.g., kafka gsm)",
    )
    parser.add_argument(
        "--kafka-host",
        type=str,
        default="http://ska-sdp-kafka.dp-shared.svc.cluster.local:9092",  # pylint: disable=line-too-long
        help="Kafka host (default: http://ska-sdp-kafka.dp-shared.svc.cluster.local)",  # pylint: disable=line-too-long
    )
    parser.add_argument(
        "--gsm-host",
        type=str,
        default=DEFAULT_GSM_HOST,  # pylint: disable=line-too-long
        help=f"GSM host (default: {DEFAULT_GSM_HOST})",  # pylint: disable=line-too-long
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    coroutine_map = {
        "kafka": lambda: _wait_for_kafka(args.kafka_host),
        "gsm": lambda: asyncio.to_thread(_wait_for_gsm, args.gsm_host),
    }
    coroutines = [coroutine_map[service]() for service in args.services]
    asyncio.run(amain(coroutines))
