Feature: RCal pipeline
    As an AIV engineer I need a bandpass calibration pipeline running in
    real-time, alongside visibility receive, writing calibration solutions to
    the data queues for the CBF beamformer.

    Scenario: Processing Components can exchange visibility streams
        Given Infrastructure for real-time data exchange
        And A processing component that can receive visibility data and generate calibration solutions
        And A processing component that can send visibility data
        When A stream of visibilities is sent by the sender processing component
        Then The final calibration solutions have the correct form
