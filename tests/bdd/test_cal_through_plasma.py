"""
plasma bdd tests
"""

import asyncio
import pathlib
import subprocess
import tempfile

import numpy
import pytest
from pytest_bdd.scenario import scenario
from pytest_bdd.steps import given, then, when
from realtime.receive.core.common import untar
from realtime.receive.modules import consumers, receiver, receivers
from realtime.receive.processors.runner import Runner
from ska_sdp_cbf_emulator import packetiser, transmitters
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.calibration.calibration_functions import (
    import_gaintable_from_hdf5,
)
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms

from realtime.calibration.processors.rcal_processor import RCalProcessor
from realtime.calibration.processors.utilities import pre_process

# pylint: disable=missing-function-docstring

PLASMA_SOCKET = "/tmp/plasma"
INPUT_FILE = "data/sim_Low_AA0.5_BP.ms"
NUM_TIMESTAMPS = 20
INPUT_GAINS = "data/sim_Low_AA0.5_BP.gains"
output_hdf5 = tempfile.mktemp(suffix=".hdf5", prefix="output_")

# Set time and frequency averaging
PREPROC_AVE_TIME = -1
PREPROC_AVE_FREQUENCY = -1


@scenario(
    "./RCal.feature",
    "Processing Components can exchange visibility streams",
)
def test_rcal_plasma():
    pass


@pytest.fixture(name="loop")
def get_loop():
    return asyncio.new_event_loop()


@pytest.fixture(name="store")
def _get_plasma_store():
    proc = subprocess.Popen(
        ["plasma_store", "-m", "20000000", "-s", PLASMA_SOCKET]
    )
    yield proc
    proc.terminate()
    proc.wait()


@given("Infrastructure for real-time data exchange")
def get_plasma_store(store):  # pylint: disable=unused-argument
    pass


@given(
    "A processing component that can receive visibility data and generate"
    " calibration solutions",
    target_fixture="processor",
)
def get_plasma_processor():
    rcal_processor = RCalProcessor(
        preproc_ave_time=PREPROC_AVE_TIME,
        preproc_ave_frequency=PREPROC_AVE_FREQUENCY,
        output_hdf5=output_hdf5,
    )
    return Runner(
        PLASMA_SOCKET,
        [rcal_processor],
        polling_rate=0.001,
        use_sdp_metadata=False,
        max_scans=1,
    )


@pytest.fixture
def receiver_ready_evt():
    return asyncio.Event()


@given(
    "A processing component that can send visibility data",
    target_fixture="receiver",
)
def get_receiver(receiver_ready_evt):
    untar(INPUT_FILE)
    config = receiver.ReceiverConfig()
    config.tm.measurement_set = INPUT_FILE
    config.scan_provider.measurement_set = INPUT_FILE
    config.uvw_engine.engine = "measures"
    config.consumer = consumers.create_config(
        name="plasma_writer",
        plasma_path=PLASMA_SOCKET,
        payloads_in_flight=NUM_TIMESTAMPS,
    )
    config.reception = receivers.create_config(
        ring_heaps=NUM_TIMESTAMPS, num_streams=1
    )
    # force a single Visibility block
    config.aggregation.time_period = 0

    return receiver.receive(config, receiver_ready_evt=receiver_ready_evt)


@when("A stream of visibilities is sent by the sender processing component")
def send_data(processor: Runner, receiver, receiver_ready_evt, loop):
    config = packetiser.SenderConfig()
    config.transmission = transmitters.create_config(
        rate=14700000, num_streams=1
    )
    config.time_interval = -1
    config.ms = packetiser.MeasurementSetDataSourceConfig(INPUT_FILE)

    sending = packetiser.packetise(config)

    async def wait_and_run(sending, evt):
        await evt.wait()
        return await sending

    # Go, go, go!
    async def run():
        tasks = [
            asyncio.create_task(coro)
            for coro in (
                wait_and_run(sending, receiver_ready_evt),
                receiver,
                processor.run(),
            )
        ]
        done, waiting = await asyncio.wait(tasks, timeout=20)
        if len(done) != len(tasks) or waiting:
            # Explicitly fail so that pytest will display stdout/stderr content
            # to aid debugging
            pytest.fail(
                "Timed out while running pipeline. See logger output for more"
                " information."
            )

    loop.run_until_complete(run())


@then("The final calibration solutions have the correct form")
def compare_gain_tables():
    vis_ms = pre_process(
        vis=create_visibility_from_ms(INPUT_FILE)[0],
        timestep=PREPROC_AVE_TIME,
        freqstep=PREPROC_AVE_FREQUENCY,
    )

    table_ms = create_gaintable_from_visibility(vis_ms, jones_type="B")
    table_rx = import_gaintable_from_hdf5(output_hdf5)

    # Check the metadata
    # ICDs specify that received time is for the end of the interval
    offset = vis_ms.integration_time.data[0] / 2.0
    assert numpy.allclose(table_rx.time.data, table_ms.time.data + offset)
    assert numpy.allclose(table_rx.frequency.data, table_ms.frequency.data)
    assert numpy.array_equal(table_rx.antenna.data, table_ms.antenna.data)
    assert numpy.array_equal(table_rx.receptor1.data, table_ms.receptor1.data)
    assert numpy.array_equal(table_rx.receptor2.data, table_ms.receptor2.data)

    # Check the gain data
    fid = open(INPUT_GAINS, "r")
    lines = fid.readlines()
    fid.close()
    for line in lines:
        pcomp = line.split(".")
        pstring = pcomp[1]
        ant = int(pcomp[2])
        pcomp = line[line.find("[") + 1 : line.find("]")].split(",")
        re = float(pcomp[0])
        im = float(pcomp[1])
        # A single gain set was used for all channels, so set all channels here
        if pstring == "g11":
            table_ms.gain.data[0, ant, :, 0, 0] = re + 1j * im
        elif pstring == "g22":
            table_ms.gain.data[0, ant, :, 1, 1] = re + 1j * im

    refant = 0
    table_ms.gain.data[0, :, :, 0, 0] *= numpy.exp(
        -1j * numpy.angle(table_ms.gain.data[0, refant, 0, 0, 0])
    )
    table_ms.gain.data[0, :, :, 1, 1] *= numpy.exp(
        -1j * numpy.angle(table_ms.gain.data[0, refant, 0, 1, 1])
    )
    table_rx.gain.data[0, :, :, 0, 0] *= numpy.exp(
        -1j * numpy.angle(table_rx.gain.data[0, refant, 0, 0, 0])
    )
    table_rx.gain.data[0, :, :, 1, 1] *= numpy.exp(
        -1j * numpy.angle(table_rx.gain.data[0, refant, 0, 1, 1])
    )

    assert numpy.allclose(table_ms.gain.data, table_rx.gain.data)

    path = pathlib.Path(output_hdf5)
    path.unlink()
