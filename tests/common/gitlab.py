"""
Simple module to check if the code is running in a CI environment.
"""

import importlib.metadata
import os


def is_in_ci():
    """
    This function checks if the code is running in a CI environment.
    """
    return os.getenv("CI") is not None


def get_commit_hash():
    """
    This function returns the commit hash of the current commit.
    """

    return os.getenv("CI_COMMIT_SHORT_SHA")


def get_image():
    """
    This function returns the name of a suitable image. Either
    from the SKA registry or from the GitLab registry.

    """

    image = "artefact.skao.int/ska-sdp-realtime-calibration"
    if is_in_ci():
        image = (
            "registry.gitlab.com/ska-telescope/sdp/"
            "ska-sdp-realtime-calibration/"
            "ska-sdp-realtime-calibration"
        )
    return image


def get_image_tag():
    """
    This function returns the image tag of the latest image.
    """
    version = importlib.metadata.version("ska-sdp-realtime-calibration")
    if is_in_ci():
        version = f"{version}-dev.c{get_commit_hash()}"

    return version
