"""
Test the import of the sky model.
This requires a connection to the
sky model service.

We are assuming this can only be done
in the DP cluster.
"""

import logging
import os
import tempfile

import yaml

from tests.common import (
    TIMEOUT,
    Comparison,
    K8sElementManager,
    get_image,
    get_image_tag,
    helm_uninstall,
    wait_for_pod,
)

LOG = logging.getLogger(__name__)


def deploy_rcal_test_model(command=None, args=None):
    """
    Deploy rcal processor to the cluster and run
    a model test.


    """

    k8s_element_manager = K8sElementManager()
    image = get_image()
    version = get_image_tag()
    values = {
        "rcal": {
            "image": image,
            "version": version,
        },
        "test-sky-model": {
            "command": command,
            "args": args,
        },
    }
    # Read the KUBE_NAMESPACE environment variable
    kube_namespace = os.getenv("KUBE_NAMESPACE")

    if kube_namespace is None:
        raise EnvironmentError(
            "KUBE_NAMESPACE environment variable is not set"
        )

    # Deploy the test
    with tempfile.NamedTemporaryFile(
        mode="w", suffix=".yaml", delete=False
    ) as file:
        yaml.safe_dump(values, file, default_flow_style=False)
        # LOG.info(yaml.dump(values))
        # filename = file.name
        pod_name = "rcal-test-model"

        k8s_element_manager.helm_install(
            release=pod_name,
            chart="tests/charts/rcal",
            namespace=kube_namespace,
            values_file=file.name,
        )
    # LOG.info("Searching for services in all namespaces")
    # for services in get_services():
    #    LOG.info("service: %s found", services.metadata.name)

    assert wait_for_pod(
        pod_name=pod_name,
        namespace=kube_namespace,
        phase="Succeeded",
        timeout=TIMEOUT,
        name_comparison=Comparison.CONTAINS,
        pod_condition=None,
    )
    helm_uninstall(release=pod_name, namespace=kube_namespace)


def test_access_sky_model():
    """
    This is the first test that should be run
    in the DP cluster.

    It just tests whether the sky model service
    can be accessed.

    """
    command = ["/bin/sh", "-c"]
    args = ["python3 /app/tests/wait_for_test_services.py --services gsm"]
    deploy_rcal_test_model(command=command, args=args)


def test_import_local_sky_model():
    """
    Test the import of the local sky model.

    This requires a connection to the
    sky model service.

    We are assuming this can only be done
    in the DP cluster.
    """

    command = ["/bin/sh", "-c"]
    # pylint: disable=line-too-long
    args = [
        ". /app/.venv/bin/activate && python3 /app/tests/sky_model/test_import_local_sky_model.py"
    ]

    deploy_rcal_test_model(command=command, args=args)


def test_create_components_from_lsm():
    """
    Test the creation of components from the local sky model.

    This requires a connection to the
    sky model service.

    We are assuming this can only be done
    in the DP cluster.
    """

    command = ["/bin/sh", "-c"]
    # pylint: disable=line-too-long
    args = [
        ". /app/.venv/bin/activate && cd /app && PYTHONPATH=/app:/app/src:/app/tests python3 -m tests.sky_model.test_create_components_from_lsm"
    ]

    deploy_rcal_test_model(command=command, args=args)
