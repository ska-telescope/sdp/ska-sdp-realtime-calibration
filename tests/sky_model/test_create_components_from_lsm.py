"""
Test creating a sky component from a local sky model
to avoid replication of code, we will import
the function from a preceding test
perhaps we should make it a fixture
"""

import asyncio
import logging

import pytest
from astropy import units
from astropy.coordinates import SkyCoord

from realtime.calibration.sky_model import convert_model_to_skycomponents
from tests.sky_model.test_import_local_sky_model import import_local_sky_model

LOG = logging.getLogger(__name__)


async def check_component_conversion():
    """Check the conversion of the model to sky components"""
    freq = [100e6, 200e6, 300e6]
    model = await import_local_sky_model()
    components = convert_model_to_skycomponents(model, freq)

    for component, source in zip(components, model.sources):
        comp = SkyCoord(component.direction)
        src = SkyCoord(
            ra=source.ra,
            dec=source.dec,
            unit=(units.deg, units.deg),  # pylint: disable=no-member
        )
        assert comp.separation(src).arcsecond < 1.0
        assert component.flux.shape == (3, 4)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(check_component_conversion())
    pytest.main([__file__])
