"""
Test the import of the local sky model
"""

import asyncio
import logging

import pytest

from realtime.calibration.sky_model import (
    LocalSkyModelRequest,
    request_local_sky_model,
)

LOG = logging.getLogger(__name__)


async def import_local_sky_model():
    """
    Import the local sky model.

    Lets centre on on 1934

    [19h39m25.026, -063d42m45.000]
    """
    example_request = LocalSkyModelRequest(
        ra=294.854275,
        dec=-63.7125,
        flux_wide=0.0,
        telescope="Murchison Widefield Array",
        fov=0.1,
    )

    LOG.info("Request: %s", example_request)

    response_model = await request_local_sky_model(example_request)

    assert response_model is not None

    # Known values to check - the model is currently returning multiple sources
    # even for this very small field of view

    known_ra = 294.088898
    known_dec = -64.433311
    tolerance = 1e-6

    # Flag to check if the known values are found
    found = False

    # Iterate through the sources to find the known values
    for source in response_model.sources:
        if source.ra == pytest.approx(
            known_ra, abs=tolerance
        ) and source.dec == pytest.approx(known_dec, abs=tolerance):
            found = True
            break

    # Assert that the known values were found
    if not found:
        LOG.error("Known RA and DEC values not found in any source")
        pytest.fail("Coordinate error")

    return response_model


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(import_local_sky_model())
    pytest.main([__file__])
