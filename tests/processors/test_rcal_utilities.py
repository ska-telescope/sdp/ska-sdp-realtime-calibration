"""
Processor helper function tests
"""

import logging

import numpy
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms

from realtime.calibration.processors.utilities import pre_process
from tests.test_utils import untar

INPUT_FILE = str(untar("data/sim_Low_AA0.5_BP.ms.tar.gz"))
logger = logging.getLogger(__name__)


def test_pre_processor():
    """Test that pre-processor functions work as expected.

    Test that time averaging works
    Test that frequency averaging works
    # Test that application of RFI masks works (not yet)
    # Test that adaptive RFI flagging works (not yet)
    """

    vis = create_visibility_from_ms(INPUT_FILE)[0]

    vpp = pre_process(vis=vis)
    assert vpp.equals(vis)

    vpp = pre_process(vis=vis, timestep=None, freqstep=None)
    assert vpp.equals(vis)

    vpp = pre_process(vis=vis, timestep=1, freqstep=1)
    assert vpp.equals(vis)

    vpp = pre_process(vis=vis, timestep=-1)
    assert len(vpp.time) == 1
    assert len(vpp.datetime) == 1
    assert len(vpp.integration_time) == 1

    vpp = pre_process(vis=vis, freqstep=-1)
    dfrequency_bf = 781.25e3
    dfrequency = vis.frequency.data[1] - vis.frequency.data[0]
    freqstep = int(numpy.round(dfrequency_bf / dfrequency))
    assert len(vpp.frequency) == len(vis.frequency) / freqstep

    vpp = pre_process(vis=vis, timestep=2)
    # check temporal coordinates and variables
    assert len(vpp.time) == len(vis.time) / 2
    assert len(vpp.datetime) == len(vis.datetime) / 2
    assert len(vpp.integration_time) == len(vis.integration_time) / 2
    assert vpp.time[0] == numpy.mean(vis.time[:2])
    assert vpp.integration_time[0] == vis.integration_time[0] * 2
    # check other coordinates and variables
    assert vpp.baselines.equals(vis.baselines)
    assert vpp.antenna1.equals(vis.antenna1)
    assert vpp.antenna2.equals(vis.antenna2)
    assert vpp.frequency.equals(vis.frequency)
    assert vpp.polarisation.equals(vis.polarisation)
    assert vpp.spatial.equals(vis.spatial)
    assert vpp.channel_bandwidth.equals(vis.channel_bandwidth)

    vpp = pre_process(vis=vis, freqstep=2)
    # check spectral coordinates and variables
    assert len(vpp.frequency) == len(vis.frequency) / 2
    assert vpp.frequency.data[0] == numpy.mean(vis.frequency.data[:2])
    assert len(vpp.channel_bandwidth) == len(vis.channel_bandwidth) / 2
    assert vpp.channel_bandwidth.data[0] == vis.channel_bandwidth.data[0] * 2
    # check other coordinates and variables
    assert vpp.time.equals(vis.time)
    assert vpp.datetime.equals(vis.datetime)
    assert vpp.integration_time.equals(vis.integration_time)
    assert vpp.baselines.equals(vis.baselines)
    assert vpp.antenna1.equals(vis.antenna1)
    assert vpp.antenna2.equals(vis.antenna2)
    assert vpp.polarisation.equals(vis.polarisation)
    assert vpp.spatial.equals(vis.spatial)
    assert vpp.uvw.equals(vis.uvw)
