"""
Test asynchronous integration of the visibility receiver and the RCalProcessor
(via plasma visibility data transfers), and of the RCalProcessor and Kafka
data queues for transfer of bandpass calibration solutions to CBF.
"""

# pylint: disable=duplicate-code

import asyncio
import logging
import subprocess
from io import BytesIO

import numpy
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaConsumer
from realtime.receive.modules import consumers, receiver, receivers
from realtime.receive.processors.runner import Runner
from realtime.receive.processors.utils.processor_utils import (
    load_base_processor_class,
)
from ska_sdp_cbf_emulator import packetiser, transmitters
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms
from ska_sdp_func_python.calibration.beamformer_utils import (
    set_beamformer_frequencies,
)

from realtime.calibration.processors.rcal_processor import RCalProcessor
from tests.test_utils import untar

INPUT_FILE = str(untar("data/sim_Low_AA0.5_BP.ms.tar.gz"))
NUM_TIMESTAMPS = 20

PLASMA_SOCKET = "/tmp/plasma"
KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "test-cbf-bandpass-updates"


PROCESSOR_CLASS = (
    "realtime.calibration.processors.rcal_processor.RCalProcessor"
)
logger = logging.getLogger(__name__)


def test_load_processor_class():
    assert load_base_processor_class(PROCESSOR_CLASS) == RCalProcessor


@pytest_asyncio.fixture(name="consumer")
async def consumer_fixture():
    """Initialise and start the Kafka consumer"""
    consumer = AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest.fixture(name="store")
def _get_plasma_store():
    """Open the plasma store"""
    with subprocess.Popen(
        ["plasma_store", "-m", "20000000", "-s", PLASMA_SOCKET]
    ) as proc:
        yield proc
        proc.terminate()
        proc.wait()


@pytest.fixture(name="receiver_ready_evt")
def receiver_ready_fixture():
    """Is the receiver ready?"""
    return asyncio.Event()


@pytest.mark.asyncio
async def test_full_integration_using_receiver(
    consumer: AIOKafkaConsumer,
    store,  # pylint: disable=unused-argument # need to start this
    receiver_ready_evt,
):
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """

    # Set up receive task to consume Kafka messages
    consumer_task = asyncio.create_task(
        get_gain_arrays(consumer, max_num_arrays=1)
    )

    # Set up the RCal processing component to receive visibility data,
    # perform bandpass calibration, and send the solutions to the data queues
    rcal_processor = RCalProcessor(
        kafka_server=KAFKA_HOST,
        kafka_topic=TEST_TOPIC,
        array="LOW",
    )

    rcal_runner = Runner(
        PLASMA_SOCKET,
        [rcal_processor],
        polling_rate=0.001,
        use_sdp_metadata=False,
        max_scans=1,
    )

    # Set up a processing component that can receive then send visibility data
    vis_receiver = await get_receiver(receiver_ready_evt=receiver_ready_evt)

    # Send the data
    await send_data(rcal_runner, vis_receiver, receiver_ready_evt)

    await check_components_and_tables(consumer_task)


async def check_components_and_tables(consumer_task):
    """Run asserts on processing components and check tables"""

    # test the state of the gain consumer task
    done, pending = await asyncio.wait([consumer_task], timeout=5)
    assert len(done) == 1
    assert len(pending) == 0
    for task in done:
        assert task.exception() is None

    # test the received messages
    table = create_gaintable_from_visibility(
        create_visibility_from_ms(INPUT_FILE)[0].isel(time=[0]),
        jones_type="B",
    )
    freq_out = set_beamformer_frequencies(table, array="LOW")
    shape_out = (
        len(table.antenna),
        len(freq_out),
        len(table.receptor1) * len(table.receptor2),
    )
    gain_arrays: list[bytes] = consumer_task.result()
    assert len(gain_arrays) == 1
    for buffer in gain_arrays:
        gains = numpy.load(BytesIO(buffer)).view(numpy.complex128)
        assert isinstance(gains, numpy.ndarray)
        assert gains.ndim == 3
        assert gains.shape == shape_out


async def get_receiver(receiver_ready_evt):
    """Configure and return the receiver"""
    config = receiver.ReceiverConfig()
    config.tm.measurement_set = INPUT_FILE
    config.scan_provider.measurement_set = INPUT_FILE
    config.uvw_engine.engine = "measures"
    config.consumer = consumers.create_config(
        name="plasma_writer",
        plasma_path=PLASMA_SOCKET,
        payloads_in_flight=NUM_TIMESTAMPS,
    )
    config.reception = receivers.create_config(
        ring_heaps=NUM_TIMESTAMPS,
        num_streams=1,
        transport_protocol="tcp",
    )
    # force a single Visibility block
    config.aggregation.time_period = 0

    return receiver.receive(config, receiver_ready_evt=receiver_ready_evt)


async def send_data(rcal_runner, vis_receiver, receiver_ready_evt):
    """Configure the packetiser and start all processing components"""
    config = packetiser.SenderConfig()
    config.transmission = transmitters.create_config(
        rate=14700000,
        num_streams=1,
        transport_protocol="tcp",
    )
    config.time_interval = -1
    config.ms = packetiser.MeasurementSetDataSourceConfig(INPUT_FILE)

    sending = packetiser.packetise(config)

    async def wait_and_run(sending, evt):
        await evt.wait()
        return await sending

    # Go, go, go!
    async def run():
        # these should all be started together
        tasks = [
            asyncio.create_task(coro)
            for coro in (
                wait_and_run(sending, receiver_ready_evt),
                vis_receiver,
                rcal_runner.run(),
            )
        ]
        done, waiting = await asyncio.wait(tasks, timeout=10)
        if len(done) != len(tasks) or waiting:
            # Explicitly fail so that pytest will display stdout/stderr
            # content to aid debugging
            pytest.fail(
                "Timed out while running pipeline. See logger output for"
                " more information."
            )

    await run()


async def get_gain_arrays(
    consumer: AIOKafkaConsumer, max_num_arrays: int
) -> list[str]:
    """Receive function for consumer task"""
    array_list = []
    async for gain_array in consumer:
        array_list.append(gain_array.value)
        if len(array_list) == max_num_arrays:
            return array_list
