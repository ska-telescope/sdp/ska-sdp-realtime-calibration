"""
Processor helper function tests
"""

import logging

import numpy
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms

from realtime.calibration.sky_model.functions import (
    generate_lsm,
    predict_model_vis,
)
from tests.test_utils import untar

INPUT_FILE = str(untar("data/sim_Low_AA0.5_BP.ms.tar.gz"))
logger = logging.getLogger(__name__)


def test_generate_lsm():
    """Test that pre-processor functions work as expected."""
    vis = create_visibility_from_ms(INPUT_FILE)[0]
    skycomponents = generate_lsm(vis=vis)
    # currently just importing a unit point source at phase centre
    assert len(skycomponents) == 1
    comp = skycomponents[0]
    assert isinstance(comp, SkyComponent)
    assert comp.direction.ra == vis.phasecentre.ra
    assert comp.direction.dec == vis.phasecentre.dec
    assert numpy.all(comp.frequency == vis.frequency.data)
    assert numpy.all(comp.flux.reshape(-1, 2, 2) == 0.5 * numpy.eye(2))
    assert comp.shape == "Point"
    assert comp.polarisation_frame == PolarisationFrame("linear")


def test_predict_model_vis():
    """Test that pre-processor functions work as expected."""
    vis = create_visibility_from_ms(INPUT_FILE)[0]
    model = predict_model_vis(vis=vis, skycomponents=generate_lsm(vis=vis))

    assert numpy.all(
        numpy.real(model.vis.data.reshape(-1, 2, 2)) == 0.5 * numpy.eye(2)
    )
    assert numpy.all(numpy.abs(numpy.imag(model.vis.data)) < 1e-7)
    assert model.time.equals(vis.time)
    assert model.datetime.equals(vis.datetime)
    assert model.integration_time.equals(vis.integration_time)
    assert model.baselines.equals(vis.baselines)
    assert model.antenna1.equals(vis.antenna1)
    assert model.antenna2.equals(vis.antenna2)
    assert model.frequency.equals(vis.frequency)
    assert model.polarisation.equals(vis.polarisation)
    assert model.spatial.equals(vis.spatial)
    assert model.uvw.equals(vis.uvw)
