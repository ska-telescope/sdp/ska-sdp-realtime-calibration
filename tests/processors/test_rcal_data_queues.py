"""
RCalProcessor Kafka data transfer tests
"""

# pylint: disable=duplicate-code

import asyncio
import logging
from io import BytesIO

import numpy
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from realtime.receive.processors.utils.processor_utils import (
    load_base_processor_class,
)
from realtime.receive.processors.utils.runner_utils import (
    arun_emulated_sdp_pipeline,
)
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms
from ska_sdp_func_python.calibration.beamformer_utils import (
    set_beamformer_frequencies,
)

from realtime.calibration.processors.rcal_processor import RCalProcessor
from tests.test_utils import untar

INPUT_MS_PATH = untar("data/sim_Low_AA0.5_BP.ms.tar.gz")
NUM_TIMESTAMPS = 20
PLASMA_SOCKET = "/tmp/plasma"
KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "test-cbf-bandpass-updates"


PROCESSOR_CLASS = (
    "realtime.calibration.processors.rcal_processor.RCalProcessor"
)
logger = logging.getLogger(__name__)


def test_load_processor_class():
    assert load_base_processor_class(PROCESSOR_CLASS) == RCalProcessor


@pytest_asyncio.fixture(name="producer")
async def producer_fixture():
    """producer test fixture"""
    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    await producer.start()
    yield producer
    await producer.stop()


@pytest_asyncio.fixture(name="consumer")
async def consumer_fixture():
    """consumer test fixture"""
    consumer = AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest.mark.asyncio
async def test_simple_kafka_transfer(
    producer: AIOKafkaProducer, consumer: AIOKafkaConsumer
):
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """

    messages = ["AAA", "BBB", "CCC"]

    # set up receive task to consume Kafka messages
    task_rx = asyncio.create_task(gain_rx(consumer, max_num_messages=3))

    async def gain_tx(producer: AIOKafkaProducer, msg: str):
        """Transmit function for producer tasks."""
        await producer.send_and_wait(TEST_TOPIC, msg.encode())

    # set up transmit tasks and produce the messages, one at a time
    for msg in messages:
        task_tx = asyncio.create_task(gain_tx(producer, msg))

        done_tx, pending_tx = await asyncio.wait([task_tx])
        assert len(done_tx) == 1
        assert len(pending_tx) == 0
        for task in done_tx:
            assert task.exception() is None

    # test the state of the receive task
    done_rx, pending_rx = await asyncio.wait([task_rx], timeout=5)
    assert len(done_rx) == 1
    assert len(pending_rx) == 0
    for task in done_rx:
        assert task.exception() is None

    # test the received messages
    received_messages: list[bytes] = task_rx.result()
    assert len(received_messages) == 3
    for expected_str, received_bytes in zip(messages, received_messages):
        assert expected_str.encode() == received_bytes


@pytest.mark.asyncio
async def test_kafka_bio_transfer(
    producer: AIOKafkaProducer, consumer: AIOKafkaConsumer
):
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """

    messages = [numpy.eye(2), numpy.eye(3), numpy.eye(4)]

    # set up receive task to consume Kafka messages
    task_rx = asyncio.create_task(gain_rx(consumer, max_num_messages=3))

    # set up transmit tasks and produce the messages, one at a time
    for msg in messages:
        bio = BytesIO()
        numpy.save(bio, msg)
        data = bio.getbuffer()
        await producer.send_and_wait(TEST_TOPIC, data)

    # test the state of the receive task
    done_rx, pending_rx = await asyncio.wait([task_rx], timeout=5)
    assert len(done_rx) == 1
    assert len(pending_rx) == 0
    for task in done_rx:
        assert task.exception() is None

    # test the received messages
    received_messages: list[bytes] = task_rx.result()
    assert len(received_messages) == 3
    for expected, received_bytes in zip(messages, received_messages):
        received = numpy.load(BytesIO(received_bytes))
        assert numpy.array_equal(received, expected)


@pytest.mark.asyncio
async def test_kafka_gain_transfer(
    producer: AIOKafkaProducer, consumer: AIOKafkaConsumer
):
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """

    table = create_gaintable_from_visibility(
        create_visibility_from_ms(str(INPUT_MS_PATH))[0].isel(time=[0]),
        jones_type="B",
    )

    # set up receive task to consume the Kafka message
    task_rx = asyncio.create_task(gain_rx(consumer, max_num_messages=1))

    # set up transmit task and send the data
    #  - The CBF beamformer requires complex arrays with dimensions
    #    [nant, nfreq, npol=4]. See ADR-58 and SP-2994.
    assert len(table.time) == 1
    nant = len(table.antenna)
    nfreq = len(table.frequency)
    npol = len(table.receptor1) * len(table.receptor2)
    assert npol == 4

    bio = BytesIO()
    numpy.save(bio, table.gain.data.reshape(nant, nfreq, npol))
    data = bio.getbuffer()
    await producer.send_and_wait(TEST_TOPIC, data)

    # test the state of the receive task
    done_rx, pending_rx = await asyncio.wait([task_rx], timeout=5)
    assert len(done_rx) == 1
    assert len(pending_rx) == 0
    for task in done_rx:
        assert task.exception() is None

    # test the received messages
    received_bytes: list[bytes] = task_rx.result()
    assert len(received_bytes) == 1
    received = numpy.load(BytesIO(received_bytes[0]))
    assert numpy.array_equal(
        received, table.gain.data.reshape(nant, nfreq, npol)
    )


@pytest.mark.asyncio
async def test_kafka_transfer_from_emulated_receiver(
    consumer: AIOKafkaConsumer,
):
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """

    # num_intervals = NUM_TIMESTAMPS
    num_intervals = 5

    # set up receive task to consume Kafka messages
    task_rx = asyncio.create_task(
        gain_rx(consumer, max_num_messages=num_intervals)
    )

    # RCalProcessor sets up a task each interval to produce the Kafka messages
    rcal_processor = RCalProcessor(
        kafka_server=KAFKA_HOST,
        kafka_topic=TEST_TOPIC,
        array="LOW",
    )

    # run plasma writer and processor til done
    await arun_emulated_sdp_pipeline(
        INPUT_MS_PATH,
        PLASMA_SOCKET,
        20000000,
        rcal_processor,
        num_scans=1,
    )

    # test the state of the receive task
    done_rx, pending_rx = await asyncio.wait([task_rx], timeout=5)
    assert len(done_rx) == 1
    assert len(pending_rx) == 0
    for task in done_rx:
        assert task.exception() is None

    # test the received messages
    table = create_gaintable_from_visibility(
        create_visibility_from_ms(str(INPUT_MS_PATH))[0].isel(time=[0]),
        jones_type="B",
    )
    freq_out = set_beamformer_frequencies(table, array="LOW")
    shape_out = (
        len(table.antenna),
        len(freq_out),
        len(table.receptor1) * len(table.receptor2),
    )
    gain_arrays: list[bytes] = task_rx.result()
    assert len(gain_arrays) == num_intervals
    for buffer in gain_arrays:
        gains = numpy.load(BytesIO(buffer)).view(numpy.complex128)
        assert isinstance(gains, numpy.ndarray)
        assert gains.ndim == 3
        assert gains.shape == shape_out


async def gain_rx(
    consumer: AIOKafkaConsumer, max_num_messages: int
) -> list[str]:
    """Receive function for consumer tasks."""
    messages = []
    async for msg in consumer:
        messages.append(msg.value)
        if len(messages) == max_num_messages:
            return messages
