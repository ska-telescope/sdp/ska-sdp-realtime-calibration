"""
RCalProcessor tests
"""

# pylint: disable=duplicate-code

import logging

import pytest
from realtime.receive.processors.utils.processor_utils import (
    load_base_processor_class,
)
from realtime.receive.processors.utils.runner_utils import (
    arun_emulated_sdp_pipeline,
)

from realtime.calibration.processors.rcal_processor import RCalProcessor
from tests.test_utils import untar

INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
PLASMA_SOCKET = "/tmp/plasma"
PROCESSOR_CLASS = (
    "realtime.calibration.processors.rcal_processor.RCalProcessor"
)
logger = logging.getLogger(__name__)


def test_load_processor_class():
    assert load_base_processor_class(PROCESSOR_CLASS) == RCalProcessor


@pytest.mark.asyncio
async def test_aa05low_single_scan():
    """Tests rcal processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """
    # run plasma writer and processor til done
    await arun_emulated_sdp_pipeline(
        INPUT_MS_PATH,
        PLASMA_SOCKET,
        20000000,
        RCalProcessor(),
        num_scans=1,
    )
