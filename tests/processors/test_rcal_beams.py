"""
Processor helper function tests
"""

import logging

import numpy
from astropy.coordinates import SkyCoord
from astropy.time import Time
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms

from realtime.calibration.sky_model.beams import GenericBeams
from tests.test_utils import untar

INPUT_FILE = str(untar("data/sim_Low_AA0.5_BP.ms.tar.gz"))
logger = logging.getLogger(__name__)


def test_beam_creation():
    """Test that pre-processor functions work as expected."""
    vis = create_visibility_from_ms(INPUT_FILE)[0]
    beams = GenericBeams(vis=vis, array="LOW")
    assert beams.beam_direction == vis.phasecentre
    assert numpy.all(beams.antenna_names == vis.configuration.names.data)
    assert beams.nantenna == len(beams.antenna_names)
    assert numpy.all(beams.frequency == vis.frequency.data)
    assert beams.nfrequency == len(beams.frequency)
    assert beams.array_location == vis.configuration.location
    assert len(beams.antenna_locations) == beams.nantenna
    assert beams.array == "low"


def test_update_beam_direction():
    """Test that pre-processor functions work as expected."""
    vis = create_visibility_from_ms(INPUT_FILE)[0]
    beams = GenericBeams(vis=vis, array="LOW")
    assert beams.beam_direction == vis.phasecentre
    direction = SkyCoord("1h", "-30d", frame="icrs")
    beams.update_beam_direction(direction)
    assert beams.beam_direction == direction


def test_array_response():
    """Test that pre-processor functions work as expected."""
    vis = create_visibility_from_ms(INPUT_FILE)[0]
    beams = GenericBeams(vis=vis, array="LOW")
    direction = SkyCoord("1h", "-30d", frame="icrs")
    time = Time(51545.4, format="mjd")
    gain = beams.array_response(direction, time)

    assert numpy.all(numpy.real(gain) == numpy.eye(2))
    assert numpy.all(numpy.imag(gain) == 0)
