"""
RCalCalibrator tests
"""

import logging

import numpy
from ska_sdp_datamodels.calibration.calibration_model import GainTable
from ska_sdp_datamodels.visibility.vis_io_ms import create_visibility_from_ms
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.calibration.calibrators.rcal_calibrator import RCalCalibrator
from tests.test_utils import untar

INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
logger = logging.getLogger(__name__)


def test_gain_table_creation():
    """Test that the GainTable generated in RCalCalibrator.process
    has the expected form.
    """

    assert isinstance(str(INPUT_MS_PATH), str)
    vis = create_visibility_from_ms(str(INPUT_MS_PATH))
    assert len(vis) == 1
    assert isinstance(vis[0], Visibility), "data type is not Visibility"

    rcal_calibrator = RCalCalibrator()
    rcal_calibrator.initialise_gaintable(vis=vis[0])
    table = rcal_calibrator.get_calibration()
    assert isinstance(table, GainTable), "return type is not GainTable"

    # some of these will depend on the calibration type
    assert table.gain.shape[0] == 1
    assert table.gain.shape[2] == vis[0].vis.shape[2]
    assert table.gain.shape[1] <= vis[0].vis.shape[1]
    assert table.gain.shape[3] <= vis[0].vis.shape[3]
    assert table.gain.shape[4] <= vis[0].vis.shape[3]

    assert table.time.data[0] == numpy.mean(vis[0].time.data)
    # probably won't work for non-bandpass calibrators
    assert (table.frequency.data == vis[0].frequency.data).all()

    assert (
        table.antenna.data
        == numpy.unique([vis[0].antenna1.data, vis[0].antenna2.data])
    ).all()


def test_gain_table_resampling():
    """Test that the GainTable generated in RCalCalibrator.process
    has the expected form.
    """

    vis = create_visibility_from_ms(str(INPUT_MS_PATH))
    assert isinstance(vis[0], Visibility), "data type is not Visibility"

    rcal_calibrator = RCalCalibrator()
    rcal_calibrator.initialise_gaintable(vis=vis[0])

    array = "MID"
    rcal_calibrator.resample(array=array)
    table = rcal_calibrator.get_calibration()
    assert isinstance(table, GainTable), "return type is not GainTable"

    assert table.gain.shape[0] == 1
    assert table.gain.shape[1] <= vis[0].vis.shape[1]
    assert table.gain.shape[3] <= vis[0].vis.shape[3]
    assert table.gain.shape[4] <= vis[0].vis.shape[3]
    if array == "LOW":
        assert (table.frequency.data % 781.25e3 == 0).all()
    else:
        width = 300.0e6 / 4096.0
        assert numpy.allclose(numpy.diff(table.frequency.data), width)
